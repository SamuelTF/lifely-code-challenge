//Include dependecies 'Run this in gitbash: npm install --s express path cookie-parser body-parser express-handlebars express-validator express-session mongodb mongoose'
const express = require('express');
const path = require('path');
const cookieParser = require('cookie-parser');
const bodyParser = require('body-parser');
const exphbs = require('express-handlebars');
const expressValidator = require('express-validator');
const session = require('express-session');
const mongo = require('mongodb');
const mongoose = require('mongoose');

// Init app
const app = express();

// Connect database
// mongoose.connect('/*login*/ ');
// const db = mongoose.connection;

// Require routes
const routes = require('./routes/web.js');

app.use('/', routes);

// View Engine
app.set('views', path.join(__dirname, 'views'));
app.engine('handlebars', exphbs({defaultLayout:'layout'}), exphbs({helpers: {
 toJSON : function(object) {
 return JSON.stringify(object);
 }
}}));


app.set('view engine', 'handlebars');

// BodyParser Middleware
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser());

// Set Static Folder
app.use(express.static(path.join(__dirname, 'public')));

// Express Session
app.use(session({
    secret: 'secret',
    saveUninitialized: true,
    resave: true
}));

// Set Port
app.set('port', (process.env.PORT || 3000));

app.listen(app.get('port'), function(){
	console.log('Server started on port '+app.get('port'));
});
