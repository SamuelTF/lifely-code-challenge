const express = require('express');
const fs = require('fs');
const bodyParser = require('body-parser');
const router = express.Router();

// Read / parse json data
const data = JSON.parse(fs.readFileSync('data.json'));

//Store data per country
var netherlands = data[0].netherlands;
var england = data[0].england;
var spain = data[0].spain;
var germany = data[0].germany;

//Merge all country arrays into 1 array.
var all_clubs = netherlands.concat(england, spain, germany);

//Get home page
router.get('/', function(req, res){
  if (req.query.query) {
    function search(club) {
      return club.name === req.query.query;
    }
    var searchResult = all_clubs.find(search);
    // console.log(searchResult)
    // The reason I don't store it in data is because if I want to loop through 1 outcome it won't work the way my I designed the frontend. + if you want to add a
    // error message you have a place to do that
    res.render('home', {searchResult});
  } else if (req.query.country) {
    var country = req.query.country;
    if (country == 'netherlands') {
      res.render('home', {data: netherlands});
      //console.log('in netherlands')
    } else if (country == 'england') {
      res.render('home', {data: england});
      //console.log('in england')
    } else if (country == 'spain') {
      res.render('home', {data: spain});
      //console.log('in spain')
    } else if (country == 'germany') {
      res.render('home', {data: germany});
      //console.log('in germany')
    } else if (country == 'all') {
      res.render('home', {data: all_clubs});
    }
  } else {
    res.render('home', {data: all_clubs}); // I think the reason why the "Everything" filter isn't working as it's the standard value of the form, so the
    // onchange function doesn't work as there is no change. (so the form never gets submitted)

  }
})

module.exports = router;
